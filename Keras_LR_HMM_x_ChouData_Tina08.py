# run batch files ; for Rona's Data
# Tina @ Feb 27, 2018

import matplotlib.pyplot as plt
import matplotlib
import scipy.io
import keras
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras import regularizers
from scipy.signal import butter, lfilter, freqz
import numpy as np
import neuroshare as ns


# to smooth stimulus signal
def smooth(x,window_len=11):
    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    w=np.ones(window_len,'d')
    y=np.convolve(w/w.sum(),s,mode='valid')
    return y

# calculate MI
def mi_quick(a,b,d,bn=25):
    if d>0: xy,_,_ = np.histogram2d(a[d:],b[:-d],bn)
    elif d<0: xy,_,_ = np.histogram2d(a[:d],b[-d:],bn)
    else: xy,_,_ = np.histogram2d(a,b,bn)
    xy /= np.sum(xy)
    px = [np.array([max(x,1e-100) for x in np.sum(xy,axis=0)])]
    py = np.transpose([[max(x,1e-100) for x in np.sum(xy,axis=1)]])
    nxy = (xy/px)/py
    nxy[nxy==0] = 1e-100
    return np.sum(xy*np.log2(nxy))

# pass filter
def butter_lowpass(data, cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='lowpass', analog=False)
    y = lfilter(b, a, data)
    return y

def butter_highpass(data, cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='highpass', analog=False)
    y = lfilter(b, a, data)
    return y

# ploting 
def timeplot(train_pred,y_train,test_pred,y_test,fileperiod):
    ############
    plottime = np.linspace(0,20,2000,endpoint=False)
    plt.axis([0,20,-1,1])
    plt.plot(plottime,train_pred[:,0][3000:5000],'r',label='train_Predict')
    plt.plot(plottime,y_train[:,0][3000:5000],'b',label='train')
    plt.legend()
    plt.title('train')
    plt.savefig(fileperiod + 'Train.png')#,transparent=True
    plt.clf()
    plt.close()
    ##############
    plt.axis([0,20,-1,1])
    plt.plot(plottime,test_pred[:,0][2000:4000],'r',label='test_Predict')
    plt.plot(plottime,y_test[:,0][2000:4000],'b',label='test')
    plt.legend()
    plt.title('test')
    plt.savefig(fileperiod + 'Test.png')#,transparent=True
    plt.clf()
    plt.close()
    return print(fileperiod,'plot timeseries')
    ###############

def Wplot(model,WindowSize,pretime,posttime,fileperiod):
    for layer in model.layers:    
        g=layer.get_config()
        h=layer.get_weights()
    Wmatrix = h[0].reshape(60, int(WindowSize))
    fig, ax = plt.subplots()
    cax = ax.imshow(Wmatrix,extent=[-pretime,posttime,1,60], aspect='auto')
    cbar = fig.colorbar(cax)
    plt.savefig(fileperiod + 'W.png')#,transparent=True
    plt.clf()
    plt.close()
    return print(fileperiod,'plot W')

def MIplot(train_pred,y_train,test_pred,y_test,fileperiod,txtfile):
    dms = range(-100,101)
    mitrainsf = [mi_quick(y_train[:,0],y_train[:,0],d) for d in dms]
    mitrainTP = [mi_quick(y_train[:,0],train_pred[:,0],d) for d in dms]
    mitestsf = [mi_quick(y_test[:,0],y_test[:,0],d) for d in dms]
    mitestTP = [mi_quick(y_test[:,0],test_pred[:,0],d) for d in dms]
    MIMax = 'MI Max in '+str(10*(mitrainsf.index(max(mitrainsf))-100))+'='+str(max(mitrainsf)*100)
    MItrain = 'MI estimate train in '+str(10*(mitrainTP.index(max(mitrainTP))-100))+'='+str(max(mitrainTP)*100)
    MItest = 'MI estimate test in '+str(10*(mitestTP.index(max(mitestTP))-100))+'='+str(max(mitestTP)*100)
    plt.plot(np.double(list(dms))*10,np.double(mitrainsf)*100,'k',label='train self')
    plt.plot(np.double(list(dms))*10,np.double(mitrainTP)*100,'r',label='train/predict')
    plt.plot(np.double(list(dms))*10,np.double(mitrainTP)*500,'--r',label='5*(train/predict)')    
    plt.plot(np.double(list(dms))*10,np.double(mitestsf)*100,'b',label='test self')
    plt.plot(np.double(list(dms))*10,np.double(mitestTP)*100,'g',label='test/predict')
    plt.plot(np.double(list(dms))*10,np.double(mitestTP)*500,'--g',label='5*(test/predict)')
    plt.text(-150, 300, MIMax, fontsize=14)
    plt.text(-150, 100, MItest, fontsize=14)
    plt.xlabel('time (ms)')
    plt.ylabel('MI (bit/s)')
    plt.legend()    
    plt.title('MI')
    plt.tight_layout()
    plt.savefig(fileperiod +'MI.png')
    plt.clf()
    plt.close()
    txtfile.write(MItrain+'\n'+MItest+'\n')
    return print('plot MI'+'\n'+MItrain+'\n'+MItest+'\n')

# Reading data 
def Ronamat_read(file,txtfile,binning_time = 0.01):
    # open matlab file 
    mat = scipy.io.loadmat(file)
    TimeStamps = mat.get('TimeStamps')[0]
    spikes=mat.get('Spikes')
    stimulation=mat.get('a_data')
    Info = mat.get('Infos')
    DataLength = np.float32(Info[0][0][7][0][0])/1000 # sec
    sampling = np.float32(Info[0][0][1][0][0]) # Hz
    triggerind = np.float16(TimeStamps[0]) # initial point, unit:s
    T_span = [triggerind+5,triggerind+285] # unit: s
    size_N = 60
    timedata = np.linspace(1., DataLength*sampling, num=int(DataLength*sampling))/sampling
    # read stimulus
    signal = stimulation[2]
    signalfiltered = butter_lowpass(signal, 50, sampling, order=5)
    signalused = signalfiltered[int(T_span[0]*sampling):int(T_span[1]*sampling)] # take the useful time period
    temp_signal = signalused.reshape(-1, int(binning_time*sampling)).mean(axis=1) # downsampling by average
    binning_signalall = (temp_signal - np.mean(temp_signal))/max(temp_signal - np.mean(temp_signal)) # rescale
    # spikeing timestamps
    channel_data = []
    for i in range(size_N):
        data=spikes[0][i][0] 
        channel_data.append(data) 
    # calculate the firing rate
    N_bin = int((T_span[1]-T_span[0])/binning_time)+1 # number of bins
    bins = np.linspace(T_span[0], T_span[1], num=N_bin) # time point of data 
    binning_rates=[]
    for i in range(size_N):
        out = np.histogram(channel_data[i], bins)
        binning_rates.append(out[0]) 
    return binning_signalall, binning_rates, size_N, T_span, binning_time

def MCD_read(file,txtfile,binning_time = 0.01):
    # arrange data
    ChannelIndex = \
    [20,18,15,14,11,9,\
    23,21,19,16,13,10,8,6,\
    25,24,22,17,12,7,5,4,\
    28,29,27,26,3,2,0,1,\
    31,30,32,33,56,57,59,58,\
    34,35,37,42,47,52,54,55,\
    36,38,40,43,46,49,51,53,\
    39,41,44,45,48,50]
    size_N = len(ChannelIndex)
    # open file using the neuroshare bindings
    fd = ns.File(file) 
    sampling = fd.entities[0].sample_rate # sampling rate = 20,000Hz
    #A2 trigger
    dummtriger = fd.entities[61] 
    trigger,timestamp,count=dummtriger.get_data()
    triggerind = next(x[0] for x in enumerate(trigger) if x[1] > 0.5) # initial point, unit: data point
    T_span = [triggerind+5*sampling,triggerind+285*sampling] # unit : data point
    # channel response
    binning_rates = []
    for ChID in ChannelIndex:
        analogdata = fd.entities[ChID]
        rawdata,timestamp,count=analogdata.get_data()
        rawdatafilter = butter_highpass(rawdata, 200, sampling, order=4)
        binning_rawdata = rawdatafilter[int(T_span[0]):int(T_span[1])].reshape(-1, int(binning_time*sampling)).mean(axis=1)
        binning_rates.append(np.array((binning_rawdata - np.mean(binning_rawdata))/max(abs(binning_rawdata - np.mean(binning_rawdata)))))
    # stimulation signal
    dummiesignal = fd.entities[62] #A3
    signal,timestamp,count=dummiesignal.get_data()
    signalfiltered = butter_lowpass(signal, 50, sampling, order=5)
    temp_signal= signalfiltered[int(T_span[0]):int(T_span[1])].reshape(-1, int(binning_time*sampling)).mean(axis=1)
    binning_signalall = (temp_signal - np.mean(temp_signal))/max(abs(temp_signal - np.mean(temp_signal)))
    T_spans = [T_span[0]/sampling,T_span[1]/sampling]
    return binning_signalall, binning_rates, size_N, T_spans, binning_time

# creat 
def SlideWindowData(binning_signalall, binning_rates, pretime,posttime,binning_time):
    size_N = 60
    bin_pre = round(pretime/binning_time) 
    bin_post  = round(posttime/binning_time)  
    WindowSize = bin_pre+bin_post+1. # W length
    TotalListNum = len(binning_signalall)- WindowSize + 1. # maximun list number of the data 
    # # smooth stimulus or not 
    # remove the extra stimulation to fit data length
    targetsignal = binning_signalall[bin_pre:-bin_post].reshape((-1,1))/max(binning_signalall[bin_pre:-bin_post]) 
    # convert the continuous data to a list, each element of the list is a windowed data 
    print('list data')
    ListData = []
    for k in range(len(targetsignal)):
        StartInd = k 
        EndInd = k + bin_pre + bin_post +1
        windowdata = []
        for i in range(size_N):
            windowdata.extend(binning_rates[i][StartInd:EndInd])
        ListData.append(windowdata) # append windowdata to a list, the len() should be the same with binning_signal
    FiringData = np.array(ListData)
    return FiringData, targetsignal

# main code
def LR_Keras(file):
    print(file)
    filesave = file[0:-4] + '_x_Wsize_'
    txtfilename = filesave+'ErrInfo.txt'
    txtfile = open(txtfilename,'w') 
    txtfile.write(txtfilename+'\n') 
    # getting spikes data and stimulation data from keys mat
    if file[-3:] == 'mcd':
        binning_signalall, binning_rates, size_N, T_span, binning_time = MCD_read(file,txtfile)
    elif file[-3:] == 'mat':
        binning_signalall, binning_rates, size_N, T_span, binning_time = Ronamat_read(file,txtfile)
    else:
        print('no matching file type. import .mcd or .mat')
    txtfile.write('binning = '+str(binning_time)+'\n')
    txtfile.write('time ='+'['+str(T_span[0])+','+str(T_span[1])+']'+'\n')
     #training window size (size of W)
    pretimearr = [1]
    posttimearr= [1]
    for pretime,posttime in zip(pretimearr,posttimearr) :
        fileperiod = filesave+'[-'+str(pretime)+','+str(posttime)+']_'
        #pretime = 1.0 # unit:s
        #posttime = 1.0 # unit:s
        txtfile.write('Wlen = [-'+str(pretime)+','+str(posttime)+']'+'\n')
        print(fileperiod)
        
        FiringData, targetsignal = SlideWindowData(binning_signalall, binning_rates, pretime,posttime,binning_time)
        
        X_train, X_test, y_train, y_test = train_test_split(FiringData, targetsignal, test_size=0.33, shuffle=False)

        WindowSize = int(round(pretime/binning_time) + round(posttime/binning_time) +1.)
        # Fitting the W matrix by keras
        print('fit W by keras')
        txtfile.write('reg = 1'+'\n')
        txtfile.write('activation = linear'+'\n')
        txtfile.write('optimizer = adam'+'\n')
        model = Sequential()

        model.add(Dense(1, 
                        input_dim=int(WindowSize*60),
                        kernel_initializer='random_uniform', 
                        activation='linear',
                        kernel_regularizer=regularizers.l2(1)))
        model.summary()

        adam_tune=keras.optimizers.Adam(lr=0.001)
        model.compile(loss='mse', optimizer=adam_tune)
        model.fit(X_train, y_train, epochs=200,validation_split=0.1, batch_size=1024,verbose=0)
        adam_tune=keras.optimizers.Adam(lr=0.0005)
        model.compile(loss='mse', optimizer=adam_tune)
        model.fit(X_train, y_train, epochs=200,validation_split=0.1, batch_size=1024,verbose=0)
        adam_tune=keras.optimizers.Adam(lr=0.0001)
        model.compile(loss='mse', optimizer=adam_tune)
        model.fit(X_train, y_train, epochs=400,validation_split=0.1, batch_size=1024,verbose=0)
        model.save(fileperiod +'model_LR.h5')

        # Make predictions using the testing set
        ErrMSE = model.evaluate(X_train, y_train,verbose=2)
        txtfile.write('MSE'+str(ErrMSE)+'\n')
        train_pred = model.predict(X_train)
        test_pred = model.predict(X_test)
        np.savez(fileperiod+'timeseries.npz',y_test=y_test,test_pred=test_pred)

        timeplot(train_pred,y_train,test_pred,y_test,fileperiod)
        Wplot(model,WindowSize,pretime,posttime,fileperiod)
        MIplot(train_pred,y_train,test_pred,y_test,fileperiod,txtfile)
        
    txtfile.close() 

import os
from os import walk
from os.path import join

# files folder
mypath = '/home/tina/disk/TensorflowCodeTina/data/Chou/20190509/sort/'
# onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
# the .mat files in all the subfolder
for path, dirs, files in walk(mypath):
    for name in files:
        file= join(path, name)
        matfile = name.split(".")[-1]
        if (("HMM" in name.split(".")[0]) or ("OU" in name.split(".")[0])) & (matfile== 'mat'):
            try:
                LR_Keras(file=file)
            except:
                print(file)







